# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Project Billing Contract Operation',
    'name_de_DE': 'Projektverwaltung Abrechnung Verträge Einsatzbericht',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Integrates billing of project operation with contracts
    ''',
    'description_de_DE': '''
    - Integriert die Abrechnung von Einsatzberichten in Projekten mit
      Verträgen.
    ''',
    'depends': [
        'project_billing_contract',
        'project_operation'
    ],
    'xml': [
        'operation.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
