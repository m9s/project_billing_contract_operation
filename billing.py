# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL


class BillingLine(ModelSQL, ModelView):
    _name = 'account.invoice.billing_line'

    def __init__(self):
        super(BillingLine, self).__init__()
        self.state = copy.copy(self.state)
        if (('waiting_authorization', 'Waiting Authorization') not in
                self.state.selection):
            self.state.selection += [
                ('waiting_authorization', 'Waiting Authorization')
            ]
        self._reset_columns()

    def _reset_state_billing_line(self, billing_line_ids):
        self.write(billing_line_ids, {'state': 'confirmed'})

BillingLine()
