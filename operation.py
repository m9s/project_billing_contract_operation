# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL, ModelWorkflow
from trytond.transaction import Transaction
from trytond.pool import Pool


class Operation(ModelWorkflow, ModelSQL, ModelView):
    _name = 'project.operation'

    def __init__(self):
        super(Operation, self).__init__()
        self.state = copy.copy(self.state)
        if ('authorized', 'Settlement authorized') not in self.state.selection:
            self.state.selection += [('authorized', 'Settlement authorized')]
        self._reset_columns()

    def confirm_billing_lines(self, operation_id):
        billing_line_obj = Pool().get('account.invoice.billing_line')
        operation = self.browse(operation_id)
        for line in operation.timesheet_lines:
            billing_line_ids = [x.id for x in line.billing_lines]
            with Transaction().set_user(0, set_context=True):
                billing_line_obj.write(billing_line_ids, {
                    'state': 'waiting_authorization'
                })

    def authorize_billing_lines(self, operation_id):
        billing_line_obj = Pool().get('account.invoice.billing_line')
        operation = self.browse(operation_id)
        for line in operation.timesheet_lines:
            billing_line_ids = [x.id for x in line.billing_lines]
            billing_line_obj.write(billing_line_ids, {'state': 'confirmed'})

    def button_draft(self, ids):
        billing_line_obj = Pool().get('account.invoice.billing_line')
        res = super(Operation, self).button_draft(ids)
        if isinstance(ids, (int, long)):
            ids = [ids]
        operations = self.browse(ids)
        for operation in operations:
            for line in operation.timesheet_lines:
                billing_line_ids = [x.id for x in line.billing_lines]
                with Transaction().set_user(0, set_context=True):
                    billing_line_obj.write(billing_line_ids, {
                        'state': 'draft'
                    })
        return res

    def wkf_operation_activity_confirm(self, operation):
        self.confirm_billing_lines(operation.id)
        super(Operation, self).wkf_operation_activity_confirm(operation)

    def wkf_operation_activity_authorize(self, operation):
        self.authorize_billing_lines(operation.id)
        self.write(operation.id, {'state': 'authorized'})


Operation()
