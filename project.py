# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL


class Work(ModelSQL, ModelView):
    _name = 'project.work'

    def _compute_billing_line_vals(self, line, defaults):
        res = super(Work, self)._compute_billing_line_vals(line, defaults)
        for billing_line in res:
            billing_line['state'] = 'draft'
        return res

Work()
